# Monitoria Sistemas Operacionais Embarcados

Repositório dedicado a monitoria da disciplina [Sistemas Operacionais Embarcados](https://github.com/DiogoCaetanoGarcia/Sistemas_Embarcados/tree/master) do curso Engenharia Eletrônica da Universidade de Brasília

## Responsáveis 

Prof. Dr: Diogo Caetano

Monitor: Gustavo Henrique

## Objetivos

- Tirar dúvidas em relação aos exercícios propostos (topicos 1, 2, 3 e 5) e ao projeto final 
- Trazer dicas, exemplos e assuntos complementares de forma a agregar com o conteúdo da disciplina

## Horário e Local

**Quintas-feiras às 12hs** no Lab SS

### Possíveis mudanças:

Nos dias de entrega de ponto de controle, condicionado à disponibilidade da sala ou de outro local apropriado, o horário da monitoria será alterado de acordo com a tabela abaixo:

|Descrição|Dia da monitoria|Dia para possível alteração|
|---|---|---|
|PC2|02/05|03/05|
|PC3|30/05|31/05|
|PC4|13/06|14/06|
|PC5|27/06|28/06|

### Assuntos complementares
- Organização do projeto e boas práticas 
- Comunicação wireless entre sistemas embarcados (uC e SBC) 
