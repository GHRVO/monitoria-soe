import cv2
import numpy as np

# Definir os IDs dos marcadores ArUco
ids = [0, 34, 30, 4]

# Criar o dicionário de marcadores ArUco
aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_6X6_250)

# Tamanho do marcador e espaçamento
marker_size = 200  # pixels
spacing = 80  # pixels

# Gerar e salvar os marcadores
markers = []
for marker_id in ids:
    marker_img = np.zeros((marker_size, marker_size), dtype=np.uint8)
    marker_img = cv2.aruco.generateImageMarker(aruco_dict, marker_id, marker_size)
    markers.append(marker_img)
    file_name = f"aruco_marker_{marker_id}.png"
    cv2.imwrite(file_name, marker_img)
    print(f"Salvo: {file_name}")

# Calcular o tamanho da colagem com o espaçamento
collage_size = marker_size * 2 + spacing
collage = np.zeros((collage_size, collage_size), dtype=np.uint8) + 255  # Fundo branco

# Colocar cada marcador em uma posição da colagem com espaçamento
collage[0:marker_size, 0:marker_size] = markers[0]
collage[0:marker_size, marker_size + spacing:collage_size] = markers[1]
collage[marker_size + spacing:collage_size, 0:marker_size] = markers[2]
collage[marker_size + spacing:collage_size, marker_size + spacing:collage_size] = markers[3]

# Salvar a colagem
collage_file_name = "aruco_markers_collage_with_spacing.png"
cv2.imwrite(collage_file_name, collage)
print(f"Colagem salva: {collage_file_name}")

# Mostrar a colagem
cv2.imshow("ArUco Markers Collage", collage)
cv2.waitKey(0)
cv2.destroyAllWindows()
