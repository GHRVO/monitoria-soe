# Dicas para o projeto final

1. O PC2 e o PC3 são os pontos de controle que ditarão o rumo do seu projeto
  - No PC2 vocês precisam mostrar para o professor que sua ideia é implementável
  - Já no PC3 vocês precisam estar bem encaminhados com a funcionalidades principal do projeto
2. Evitem usar hardwares muito complexos, ou então que não tenha um suporte e materiais de fácil acesso.
    Ex: 
    - Módulo WiFi usb desconhecido
    - Motor de passo, sistema de alimentação complexo ---> PI2
3. Aproveitem a disciplina para aprender/exercitar sobre git, comunicação wireless, protocolos de comunicação serial, processamento de sinais. 


