import requests

# URL do endpoint
url = 'http://192.168.15.187:5000/save_content'

# Conteúdo que você quer salvar
payload = {'content': 'Este é o conteúdo que eu quero salvar.'}

# Nome do dispositivo no cabeçalho
headers = {'device': 'computador-gustavo'}

# Enviar solicitação POST para o endpoint
response = requests.post(url, json=payload, headers=headers)

# Verificar a resposta
print(response.json())
