from flask import Flask, request, jsonify
import os

app = Flask(__name__)

def save_content_to_file(content, file_path):
    with open(file_path, 'w') as file:
        file.write(content)

@app.route('/save_content', methods=['POST'])
def handle_save_content():
    content = request.json.get('temperatura')
    device_name = request.headers.get('device')
    
    if content is None:
        return jsonify({'error': 'No content provided'}), 400
    
    if device_name is None:
        return jsonify({'error': 'No device name provided in headers'}), 400
    
    # Salvar o conteúdo em um arquivo com o nome do dispositivo no mesmo diretório do programa
    file_path = os.path.join(os.path.dirname(__file__), f'{device_name}.txt')
    save_content_to_file(content, file_path)
    
    return jsonify({'message': f'Content saved successfully to {device_name}.txt'}), 200

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
