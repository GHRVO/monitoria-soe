#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

void sendPost(String payload);

const char* ssid = "";        // SSID da sua rede Wi-Fi
const char* password = "";   // Senha da sua rede Wi-Fi
const char* server_address = "http://192.168.15.187:5000/save_content";  // Endereço do servidor Flask

void setup() {
  Serial.begin(115200);
  delay(100);

  // Conectar ao Wi-Fi
  Serial.println();
  Serial.print("Conectando ao Wi-Fi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectado");
  Serial.println("Endereço IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  JsonDocument jsonDocument;
  
  char temperatura[4];

  sprintf(temperatura, "%d", random(10,40));

  jsonDocument["temperatura"] = temperatura;

  String payload;
  serializeJson(jsonDocument, payload);

  sendPost(payload);

  delay(5000);
}

void sendPost(String payload) {
  HTTPClient http;

  char mac[10];
  sprintf(mac, "%010lld", (ESP.getEfuseMac() / 100000));


  http.begin(server_address);
  http.addHeader("Content-Type", "application/json");
  http.addHeader("device", mac);  // Nome do dispositivo
  int httpResponseCode = http.POST(payload);

  if (httpResponseCode > 0) {
    String response = http.getString();
    Serial.print("Resposta do servidor: ");
    Serial.println(response);
  } else {
    Serial.print("Erro na solicitação: ");
    Serial.println(httpResponseCode);
  }

  http.end();
}
