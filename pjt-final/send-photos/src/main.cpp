#include "../include/telegram-funcs.h"
#include <tgbot/tgbot.h>
#include <cstdio>
#include <csignal>
#include <unistd.h> 
#include <cstring>  
#include <iostream>
#include <ctime> 
#include"../include/secrets.h" // coloque seu define para o token --> #define TOKEN "codigo"

void messageCheck(int *pipe, pid_t pid_filho);
void takePicture(int *pipe);

int main() {
    pid_t pid;   
    int fd[2];  
    pipe(fd); 
    pid = fork(); 
    if(pid == 0) {
        takePicture(fd);
    } else {
        messageCheck(fd, pid);
    }
    return 0;
}

void messageCheck(int *pipe, pid_t pid_filho) {

    TelegramFuncs telegramBot(TOKEN);
    TgBot::TgLongPoll longPoll(*telegramBot.getBot());
    bool startedFlag = false;
    while (true) {
        longPoll.start();

        std::string lastMessage = telegramBot.getLastMessage();

        if (!strcmp(telegramBot.getLastMessage().c_str(), "Iniciar") && !startedFlag) {
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Iniciando envio de fotos");
            telegramBot.setMessage("");
            startedFlag = true;
            printf("Iniciando envio de fotos\n");
        }     

        if (startedFlag) {   
            char filename[64];
            if(read(pipe[0], filename, sizeof(filename)) > 0) {
                printf("%s\n", filename);
                telegramBot.sendPhoto(telegramBot.getLastChatID(), filename);
                printf("Nova foto enviada: %s\n", filename);
            }
        }  

        if (!strcmp(telegramBot.getLastMessage().c_str(), "Encerrar") && startedFlag) {   
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Encerrando programa");
            telegramBot.setMessage("");
            printf("Encerrando o programa\n");
            kill(pid_filho, SIGKILL);
            return;
        }  
    }
}

void takePicture(int *pipe) {
    printf("Iniciando takePicture\n");
    char filename[64];
    char buffer[128];
    while(true) {
        
        time_t now = time(NULL);
        struct tm *t = localtime(&now);

        char time_str[100];
        strftime(time_str, sizeof(time_str), "%Y-%m-%d_%H-%M-%S", t);

        snprintf(filename, sizeof(filename), "Pictures/%s.jpg", time_str);

        snprintf(buffer, sizeof(buffer),"fswebcam -r 640x480 --jpeg 85 -D 1 ./%s", filename);
        system(buffer);

        printf("Nova foto criada: %s\n", filename);
        write(pipe[1], filename, sizeof(filename));

        sleep(30);
    
    }
}

