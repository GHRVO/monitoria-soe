#!/bin/bash

# Instalação das dependências necessárias
echo "Instalando dependências..."
sudo apt-get update
sudo apt-get install -y cmake libcurl4-openssl-dev libboost-all-dev

# Clonar o repositório e compilar
echo "Clonando o repositório tgbot-cpp..."
git clone https://github.com/reo7sp/tgbot-cpp.git

cd tgbot-cpp

mkdir build
cd build

echo "Configurando com CMake..."
cmake ..

echo "Compilando..."
make

echo "Instalando..."
sudo make install

cd ../..

echo "tgbot-cpp foi instalado com sucesso."
