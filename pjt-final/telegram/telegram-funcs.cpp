#include "telegram-funcs.h"
#include <iostream>
#include <tgbot/tgbot.h>
#include <fstream>
#include <sys/stat.h> // Para mkdir
#include <errno.h> // Para tratamento de erros do POSIX

// Sempre que uma nova mensagem chega a função processMessage é acionada
TelegramFuncs::TelegramFuncs(const std::string &token) : botToken(token)
{
    bot = new TgBot::Bot(token);
    bot->getEvents().onAnyMessage([this](TgBot::Message::Ptr message) {
        processMessage(message, *this);
    });
}

TelegramFuncs::~TelegramFuncs()
{
    delete bot;
}

TgBot::Bot* TelegramFuncs::getBot() const
{
    return bot;
}

// Função para enviar mensagens
void TelegramFuncs::sendMessage(const std::string &chatId, const std::string &text)
{
    bot->getApi().sendMessage(chatId, text);
}

// Função para enviar arquivos genéricos
void TelegramFuncs::sendFile(const std::string &chatId, const std::string &filePath)
{
    bot->getApi().sendDocument(chatId, TgBot::InputFile::fromFile(filePath, "application/octet-stream"));
}

// Função para enviar fotos
void TelegramFuncs::sendPhoto(const std::string &chatId, const std::string &filePath)
{
    bot->getApi().sendPhoto(chatId, TgBot::InputFile::fromFile(filePath, "image/jpeg"));
}

// Retornar a ultima mensagem recebida
std::string TelegramFuncs::getLastMessage()
{
    return receivedMessage;
}

// Alterar o conteudo do buffer que guarda a ultima mensagem recebida
void TelegramFuncs::setMessage(std::string message)
{
    receivedMessage = message;
}

// Retornar o chat id da ultima mensagem recebida
std::string TelegramFuncs::getLastChatID()
{
    return chatID;
}

// Alterar o conteudo do buffer que guarda o id da ultima mensagem recebida
void TelegramFuncs::setChatID(int64_t id)
{
    chatID = std::to_string(id);
}

// Sempre que uma nova mensagem chega, printar na tela e salvar o chat id e a mensagem em buffers
void processMessage(TgBot::Message::Ptr message, TelegramFuncs& telegramFuncs)
{
    printf("Nova mensagem recebida de chat ID: %ld\n", message->chat->id);
    if (!message->text.empty()) {
        printf("Texto da mensagem: %s\n", message->text.c_str());
        telegramFuncs.setMessage(message->text.c_str());
        printf("Hora da mensagem: %d\n", message->date);
    } else if (!message->photo.empty()) {
        // Tratamento de fotos
        TgBot::PhotoSize::Ptr photo = message->photo.back();
        std::string fileId = photo->fileId;
        TgBot::File::Ptr file = telegramFuncs.getBot()->getApi().getFile(fileId);
        std::string filePath = "downloads/" + fileId + ".jpg";
        
        // Criar o diretório downloads se não existir
        if (mkdir("downloads", 0777) == -1 && errno != EEXIST) {
            perror("Erro ao criar diretório");
            return;
        }
        
        printf("Baixando foto para: %s\n", filePath.c_str());
        
        std::ofstream ofs(filePath, std::ios::binary);
        if (!ofs) {
            printf("Erro ao abrir o arquivo para escrita: %s\n", filePath.c_str());
            return;
        }
        
        auto fileData = telegramFuncs.getBot()->getApi().downloadFile(file->filePath);
        ofs.write(reinterpret_cast<const char*>(fileData.data()), fileData.size());
        ofs.close();
        
        if (!ofs) {
            printf("Erro ao escrever o arquivo: %s\n", filePath.c_str());
        } else {
            printf("Foto recebida e salva: %s\n", filePath.c_str());
        }
    } else if (message->document != nullptr) {
        // Tratamento de documentos
        std::string fileId = message->document->fileId;
        TgBot::File::Ptr file = telegramFuncs.getBot()->getApi().getFile(fileId);
        std::string filePath = "downloads/" + message->document->fileName;
        
        // Criar o diretório downloads se não existir
        if (mkdir("downloads", 0777) == -1 && errno != EEXIST) {
            perror("Erro ao criar diretório");
            return;
        }
        
        printf("Baixando arquivo para: %s\n", filePath.c_str());
        
        std::ofstream ofs(filePath, std::ios::binary);
        if (!ofs) {
            printf("Erro ao abrir o arquivo para escrita: %s\n", filePath.c_str());
            return;
        }
        
        auto fileData = telegramFuncs.getBot()->getApi().downloadFile(file->filePath);
        ofs.write(reinterpret_cast<const char*>(fileData.data()), fileData.size());
        ofs.close();
        
        if (!ofs) {
            printf("Erro ao escrever o arquivo: %s\n", filePath.c_str());
        } else {
            printf("Documento recebido e salvo: %s\n", filePath.c_str());
        }
    }
    telegramFuncs.setChatID(message->chat->id);
}
