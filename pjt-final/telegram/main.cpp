#include "telegram-funcs.h"
#include <tgbot/tgbot.h>
#include <cstdio>
#include <csignal>

#define TOKEN "YOUR TOKEN"

int main() {
    TelegramFuncs telegramBot(TOKEN);
    TgBot::TgLongPoll longPoll(*telegramBot.getBot());

 while (true) {
        longPoll.start();

        if (!strcmp(telegramBot.getLastMessage().c_str(), "Iniciar"))
        {   
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Nova conversa iniciada");
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Escolha:\n 1 - Começar o projeto\n 2 - Receber uma foto\n 3 - Receber um arquivo\n 4 - Encerrar o projeto");
            telegramBot.setMessage("");
        }    
        else if (!strcmp(telegramBot.getLastMessage().c_str(), "1"))
        {
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Projeto iniciado!");
            telegramBot.setMessage("");

        }
        else if (!strcmp(telegramBot.getLastMessage().c_str(), "2"))
        {
            std::string photoPath = "downloads/unb.jpg"; 
            telegramBot.sendPhoto(telegramBot.getLastChatID(), photoPath);
            telegramBot.setMessage("");
        }
        else if (!strcmp(telegramBot.getLastMessage().c_str(), "3"))
        {
            std::string documentPath = "downloads/README.pdf"; 
            telegramBot.sendFile(telegramBot.getLastChatID(), documentPath);
            telegramBot.setMessage("");
        }       
        else if (!strcmp(telegramBot.getLastMessage().c_str(), "4"))
        {
            telegramBot.sendMessage(telegramBot.getLastChatID(), "Projeto Encerado!");
            telegramBot.setMessage("");
            break;
        }
    }


    return 0;
}
