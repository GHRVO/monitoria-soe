# Exemplo de Telegram Bot

## Instale as dependencias 

    chmod +x install.sh
    ./install.sh

## Descrição

- Como enviar mensagem pelo bot no chat
- Como ser notificado quando uma nova mensagem for recebida
- Como enviar arquivos para o bot e recebê-lo no código
- Como enviar arquivos pelo bot e receber no chat
- Como criar um menu de opções no bot e pedir para o usuário escolher

## Referencias 

- [Telegram API](https://core.telegram.org/api)
- [tgbot-cpp](https://github.com/reo7sp/tgbot-cpp)